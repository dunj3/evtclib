//! Raw evtc structs.
//!
//! This module contains the translated definitions from arcdps's C structs.
use num_derive::FromPrimitive;

use std::hash::Hash;

/// The "friend or foe" enum.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, Hash, FromPrimitive)]
pub enum IFF {
    /// Green vs green, red vs red.
    Friend,
    /// Green vs red.
    Foe,
    /// Something very wrong happened.
    Unknown,
    /// Field is not used in this kind of event.
    #[default]
    None,
}

/// Combat result (physical)
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, Hash, FromPrimitive)]
pub enum CbtResult {
    /// Good physical hit
    Normal,
    /// Physical hit was a critical hit
    Crit,
    /// Physical hit was a glance
    Glance,
    /// Physical hit was blocked (e.g. Shelter)
    Block,
    /// Physical hit was evaded (e.g. dodge)
    Evade,
    /// Physical hit interrupted something
    Interrupt,
    /// Physical hit was absorbed (e.g. invulnerability)
    Absorb,
    /// Physical hit missed
    Blind,
    /// Physical hit was the killing blow
    KillingBlow,
    /// Hit was downing hit.
    Downed,
    /// Hit was breakbar damage.
    Breakbar,
    /// Field is not used in this kind of event.
    #[default]
    None,
}

/// Combat activation
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, Hash, FromPrimitive)]
pub enum CbtActivation {
    /// Field is not used in this kind of event.
    #[default]
    None,
    /// Activation without quickness
    Normal,
    /// Activation with quickness
    Quickness,
    /// Cancel with reaching channel time
    CancelFire,
    /// Cancel without reaching channel time
    CancelCancel,
    /// Animation completed fully
    Reset,
}

/// Combat state change
///
/// The referenced fields are of the [`CbtEvent`](struct.CbtEvent.html)
/// struct.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, Hash, FromPrimitive)]
pub enum CbtStateChange {
    /// Field is not used in this kind of event.
    #[default]
    None,
    /// `src_agent` entered combat.
    ///
    /// * `dst_agent` specifies the agent's subgroup.
    EnterCombat,
    /// `src_agent` left combat.
    ExitCombat,
    /// `src_agent` is now alive.
    ChangeUp,
    /// `src_agent` is now dead.
    ChangeDead,
    /// `src_agent` is now downed.
    ChangeDown,
    /// `src_agent` is now in game tracking range.
    Spawn,
    /// `src_agent` is no longer being tracked.
    Despawn,
    /// `src_agent` has reached a health marker.
    ///
    /// * `dst_agent` will be set to the new health percentage, multiplied by
    ///   10000.
    HealthUpdate,
    /// Log start.
    ///
    /// * `value` is the server unix timestamp.
    /// * `buff_dmg` is the local unix timestamp.
    /// * `src_agent` is set to `0x637261` (arcdps id)
    LogStart,
    /// Log end.
    ///
    /// * `value` is the server unix timestamp.
    /// * `buff_dmg` is the local unix timestamp.
    /// * `src_agent` is set to `0x637261` (arcdps id)
    LogEnd,
    /// `src_agent` swapped the weapon set.
    ///
    /// * `dst_agent` is the current set id (0/1 for water sets, 4/5 for land
    ///   sets)
    WeapSwap,
    /// `src_agent` has had it's maximum health changed.
    ///
    /// * `dst_agent` is the new maximum health.
    MaxHealthUpdate,
    /// `src_agent` is the agent of the recording player.
    PointOfView,
    /// `src_agent` is the text language.
    Language,
    /// `src_agent` is the game build.
    GwBuild,
    /// `src_agent` is the server shard id.
    ShardId,
    /// Represents the reward (wiggly box)
    ///
    /// * `src_agent` is self
    /// * `dst_agent` is reward id.
    /// * `value` is reward type.
    Reward,
    /// Combat event that will appear once per buff per agent on logging start (zero duration,
    /// buff==18)
    BuffInitial,
    /// `src_agent` changed position.
    ///
    /// * `dst_agent` is a 2-element float array (x, y).
    /// * `value` is a single float (z).
    Position,
    /// `src_agent` changed velocity.
    ///
    /// * `dst_agent` is a 2-element float array (x, y).
    /// * `value` is a single float (z).
    Velocity,
    /// `src_agent` changed the direction that they're facing.
    ///
    /// * `dst_agent` is a 2-element float array (x, y).
    Facing,
    /// `src_agent` changed team.
    ///
    /// * `dst_agent` is the new team id
    TeamChange,
    /// `src_agent` is an attacktarget, `dst_agent` is the parent agent (gadget type), `value` is the current targetable state
    AttackTarget,
    /// `dst_agent` is the new target-able state (0 = no, 1 = yes. default yes)
    Targetable,
    /// Information about the map that the log was done on.
    ///
    /// * `src_agent` is map id
    MapId,
    /// internal use by arcDPS, won't see anywhere
    ReplInfo,
    /// `src_agent` is agent with buff, `dst_agent` is the stackid marked active
    StackActive,
    /// `src_agent` is agent with buff, `value` is the duration to reset to (also marks inactive),
    /// `pad61-` is the stackid
    StackReset,
    /// Information about the guild.
    ///
    /// * `src_agent` is the agent
    /// * `dst_agent` through `buff_dmg` is 16 byte guild id (client form, needs minor rearrange
    ///   for api form)
    Guild,
    /// `is_flanking` = probably invuln, `is_shields` = probably invert, `is_offcycle` = category, `pad61` = stacking type, `src_master_instid` = max stacks (not in realtime)
    BuffInfo,
    /// `(float*)&time [9]`: type attr1 attr2 param1 param2 param3 trait_src trait_self, `is_flanking` = !npc, `is_shields` = !player, `is_offcycle` = break (not in realtime, one per formula)
    BuffFormula,
    /// `(float*)&time [9]`: recharge range0 range1 tooltiptime (not in realtime)
    SkillInfo,
    /// `src_agent` = action, `dst_agent` = at millisecond (not in realtime, one per timing)
    SkillTiming,
    /// `src_agent` is agent, `value` is u16 game enum (active, recover, immune, none)
    BreakbarState,
    /// `src_agent` is agent, `value` is float with percent
    BreakbarPercent,
    /// `time` is the start of the error string.
    ///
    /// In modern arcdps, this is called `CBTS_INTEGRITY`, but we leave it for backward
    /// compatibility.
    Error,
    /// `src_agent` is the agent, `value` is the tag id
    ///
    /// In modern arcdps, this is called `CBTS_MARKER`, but we leave it for backward compatibility.
    Tag,
    /// `src_agent` is at barrier percent, `dst_agent` is the percentage times 10000 (so 99.5%
    /// will be 9950).
    BarrierUpdate,
    /// `src_agent` is the species id of the agent that triggered the reset.
    StatReset,
    /// For extension use, not managed by arcdps.
    Extension,
    /// For events that are deemed unsafe to report real-time.
    ///
    /// Should not appear in evtc files.
    ApiDelayed,
    /// Map instance start.
    ///
    /// `src_agent` are milliseconds since the instance was created.
    InstanceStart,
    /// Tick health.
    ///
    /// `src_agent` is 25 - tickrate, when tickrate <= 20
    RateHealth,
    /// No longer used.
    Last90BeforeDown,
    /// No longer used.
    Effect,
    /// content id to guid association for volatile types
    ///
    /// src_agent: (uint8_t*)&src_agent is uint8_t[16] guid of content
    /// overstack_value: is of enum contentlocal
    IdToGuid,
    /// Log boss agent changed.
    ///
    /// src_agent: species id of agent
    /// dst_agent: related to agent
    /// value: as uint32_t, server unix timestamp
    LogNpcUpdate,
    /// Internal use.
    IdleEvent,
    /// For extension use.
    ExtensionCombat,
    /// Fractal scale.
    ///
    /// `src_agent` will be the fractal scale.
    FractalScale,
    /// Play graphical effect.
    ///
    /// src_agent: related to agent
    /// dst_agent: effect at location of agent (if applicable)
    /// value: (float*)&value is float[3], location x/y/z (if not at agent location)
    /// iff: (uint32_t*)&iff is uint32_t[1], effect duration
    /// buffremove: (uint32_t*)&buffremove is uint32_t[1], trackable id of effect. id dst_agent and location is 0/0/0, effect was stopped
    /// is_shields: (int16_t*)&is_shields is int16_t[3], orientation x/y/z, values are original*1000
    /// is_flanking: effect is on a non-static platform
    Effect2,
    /// Ruleset for self.
    ///
    /// `src_agent` has bit 0 if PvE, bit 1 if WvW and bit 2 if PvP.
    Ruleset,
    /// Squad ground markers.
    ///
    /// src_agent: (float*)&src_agent is float[3], x/y/z of marker location. if values are all zero or infinity, this marker is removed
    /// skillid: index of marker eg. 0 is arrow
    SquadMarker,
    /// Arcdps build info.
    ///
    /// `src_agent` is a null-terminated string matching the full build string.
    ArcBuild,
    /// Glider status change.
    ///
    /// `src_agent` is the agent, `value` 1 is deployed and 0 is stowed.
    Glider,
    /// `src_agent` is the agent, `value` is the remaining duration.
    Stunbreak,
}

/// Combat buff remove type
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, Hash, FromPrimitive)]
pub enum CbtBuffRemove {
    /// Field is not used in this kind of event.
    #[default]
    None,
    /// All stacks removed.
    All,
    /// Single stack removed.
    ///
    /// Disabled on server trigger, will happen for each stack on cleanse.
    Single,
    /// Autoremoved by OOC or allstack.
    ///
    /// (Ignore for strip/cleanse calc, use for in/out volume)-
    Manual,
}

/// Custom skill ids
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, FromPrimitive)]
pub enum CbtCustomSkill {
    /// Not custom but important and unnamed.
    Resurrect = 1066,
    /// Personal healing only.
    Bandage = 1175,
    /// Will occur in is_activation==normal event.
    Dodge = 65001,
}

/// Language
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, Hash, FromPrimitive)]
pub enum Language {
    /// English.
    #[default]
    Eng = 0,
    /// French.
    Fre = 2,
    /// German.
    Gem = 3,
    /// Spanish.
    Spa = 4,
}

/// Buff formula attributes.
///
/// Variants prefixed with `Custom` are not native to the game client but rather variants added by
/// arcdps.
///
/// The suffix `Inc` indicates an increase, whereas the suffix `Rec` stands for received.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, FromPrimitive)]
pub enum Attribute {
    None,
    Power,
    Precision,
    Toughness,
    Vitality,
    Ferocity,
    Healing,
    Condition,
    Concentration,
    Expertise,
    CustomArmor,
    CustomAgony,
    CustomStatInc,
    CustomFlatInc,
    CustomPhysInc,
    CustomCondInc,
    CustomPhysRec,
    CustomCondRec,
    CustomAttackSpeed,
}

/// Categories for [`BuffInfo`][CbtStateChange::BuffInfo] events.
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, FromPrimitive)]
pub enum BuffCategory {
    Boon = 0,
    Any = 1,
    Condition = 2,
    Food = 4,
    Upgrade = 6,
    Boost = 8,
    Trait = 11,
    Enhancement = 13,
    Stance = 16,
}

/// A combat event.
///
/// This event combines both the old structure and the new structure. Fields not
/// present in the source are zeroed. When dealing with events, always make sure
/// to check the header.revision tag.
///
/// For conflicting data types, the bigger one is chosen (e.g. u32 over u16).
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, Hash, Default)]
pub struct CbtEvent {
    /// System time since Windows was started, in milliseconds.
    pub time: u64,
    /// Unique identifier of the source agent.
    pub src_agent: u64,
    /// Unique identifier of the destination agent.
    pub dst_agent: u64,
    /// Event-specific value.
    pub value: i32,
    /// Estimated buff damage. Zero on application event.
    pub buff_dmg: i32,
    /// Estimated overwritten stack duration for buff application.
    pub overstack_value: u32,
    /// Skill id.
    pub skillid: u32,
    /// Agent map instance id.
    pub src_instid: u16,
    /// Agent map instance id.
    pub dst_instid: u16,
    /// Master source agent map instance id if source is a minion/pet.
    pub src_master_instid: u16,
    /// Master destination agent map instance id if destination is a minion/pet.
    pub dst_master_instid: u16,
    pub iff: IFF,
    /// Buff application, removal or damage event.
    pub buff: u8,
    pub result: CbtResult,
    pub is_activation: CbtActivation,
    pub is_buffremove: CbtBuffRemove,
    /// Source agent health was over 90%.
    pub is_ninety: bool,
    /// Target agent health was under 90%.
    pub is_fifty: bool,
    /// Source agent was moving.
    pub is_moving: bool,
    pub is_statechange: CbtStateChange,
    /// Target agent was not facing source.
    pub is_flanking: bool,
    /// All or part damage was vs. barrier/shield.
    pub is_shields: bool,
    /// False if buff dmg happened during tick, true otherwise.
    pub is_offcycle: bool,
    /// Padding for some events.
    pub padding_end: u32,
}

/// An agent.
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Agent {
    /// Agent id.
    pub addr: u64,
    /// Agent profession id.
    pub prof: u32,
    /// Agent elite specialization.
    pub is_elite: u32,
    /// Toughnes.
    pub toughness: i16,
    /// Concentration.
    pub concentration: i16,
    /// Healing.
    pub healing: i16,
    /// Condition
    pub condition: i16,
    /// Name/Account combo field.
    pub name: [u8; 64],
}

impl Agent {
    /// Checks whether this agent is a gadget.
    ///
    /// Gadgets are entities spawned by some skills, like the "Binding Roots"
    /// spawned by Entangle.
    pub fn is_gadget(&self) -> bool {
        self.is_elite == u32::MAX && (self.prof & 0xffff_0000) == 0xffff_0000
    }

    /// Checks whether this agent is a character.
    ///
    /// Characters are entities like clones, pets, minions, spirits, but also
    /// minis.
    pub fn is_character(&self) -> bool {
        self.is_elite == u32::MAX && (self.prof & 0xffff_0000) != 0xffff_0000
    }

    /// Checks whether this agent is a player.
    pub fn is_player(&self) -> bool {
        self.is_elite != u32::MAX
    }
}

// We need to implement this manually, as our array is bigger than 32.

impl Default for Agent {
    fn default() -> Self {
        Self {
            addr: Default::default(),
            prof: Default::default(),
            is_elite: Default::default(),
            toughness: Default::default(),
            concentration: Default::default(),
            healing: Default::default(),
            condition: Default::default(),
            name: [0; 64],
        }
    }
}

/// A skill.
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Skill {
    /// Skill id.
    pub id: i32,
    /// Skill name.
    pub name: [u8; 64],
}

impl Skill {
    /// Return the name of the skill as a `String`.
    ///
    /// Returns `None` if the name is not valid UTF-8.
    pub fn name_string(&self) -> Option<String> {
        let bytes = self
            .name
            .iter()
            .cloned()
            .take_while(|b| *b != 0)
            .collect::<Vec<_>>();
        String::from_utf8(bytes).ok()
    }
}

impl Default for Skill {
    fn default() -> Self {
        Self {
            id: Default::default(),
            name: [0; 64],
        }
    }
}
